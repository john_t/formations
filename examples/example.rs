use formations::FormElement;

fn main() -> std::io::Result<()> {
    let mut form: Vec<FormElement<_, bool>> = Vec::new();
    for i in 0..100_usize {
        form.push(FormElement::new(
            char::from(65 + (i % 26_usize) as u8),
            None,
        ));
    }
    println!("{}", FormElement::run(&form)?.name);

    Ok(())
}
