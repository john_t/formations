# Formations

Formations is a rust library to allow you to easily create terminal forms, where a user is presented with multiple options and has to pick one. It looks something like:

```txt
1) Option 1  2) Option 2
3) Option 3  4) Option 4
```

It can be programmed as follows:

```rust
use formations::FormElement;

fn main() -> std::io::Result<()> {
    println!("Pick your favourite pet.");
    let mut form = vec![
        FormElement::new("cat", "Small mammal with fur"),
        FormElement::new("dog", "Likes a bone"),
        FormElement::new("mouse", "Likes cheese"),
    ];
    println!("{}", FormElement::run(&form)?.description.unwrap());

    Ok(())
}
```
